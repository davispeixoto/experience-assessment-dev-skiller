const Properties = require('./properties');
const Kata = require('../src/index');
const Assert = require('assert');

const EXPLORADOR         = new Properties ("EXPLORADOR", "string");
const BOLSA_DE_ITENS     = new Properties ("bolsaDeItens", "object");
const MOEDAS             = new Properties ("moedas", "number");
const ENERGIA            = new Properties ("energia", "number");
const RECOMPENSAS        = new Properties ("recompensas", "object");

const CORRECT_PROPERTIES = [EXPLORADOR, BOLSA_DE_ITENS, MOEDAS, ENERGIA, RECOMPENSAS];

/**
 * Testando se variáveis foram instanciadas corretamente
 */
describe('Index', function() {
    describe('validation', function() {
        CORRECT_PROPERTIES.forEach(prop => {
            it (`A Variável ${prop.name} foi instanciada corretamente`, function() {
                let exists      = Kata.properties.hasOwnProperty(prop.name);
                let typeCheck   = typeof Kata.properties[prop.name];
    
                // Assert.strictEqual(exists, true, `A Variável ${prop.name} não foi instanciada.`);
                // Assert.strictEqual(typeCheck, prop.type, `A Variável ${prop.name} não é do tipo correto.`);
                expect(exists).to.equal(true);
                expect(typeCheck).to.equal(prop.type);
            });
        });
        // it("A variável EXPLORADOR foi instanciada corretamente", function () {
        //     let exists = Kata.properties.hasOwnProperty('EXPLORADOR');
        //     let typeCheck = typeof Kata.properties[EXPLORADOR];

        //     Assert.strictEqual(exists, true, 'A variável EXPLORADOR não foi instanciada.');
        //     Assert.strictEqual(typeCheck, "string", 'A variável EXPLORADOR não é do tipo correto.');
        // });
    });
});