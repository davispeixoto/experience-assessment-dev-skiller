class Properties {
    constructor (name, type){
        this._name = name;
        this._type = type;
    }
    get name () { return this._name; }
    get type () { return this._type; }

    set name (name) { return this._name = name; }
    set type (type) { return this._type = type; }
}

module.exports = Properties;